(function($){
  'use strict';
  
    jQuery(document).ready(function($){
     // Defining a function to set size for #hero 
        /* ========== Background image height equal to the browser height.==========*/
      $('header').css({ 'height': $(window).height() });
         $(window).on('resize', function() {
            $('header').css({ 'height': $(window).height() });
         });

    });


    /*------Contact form-----*/
    $('#contactform').bootstrapValidator({
      container: 'tooltip',
      feedbackIcons: {
        valid: 'fa fa-check',
        invalid: 'fa fa-times',
        validating: 'fa fa-refresh'
      },
      fields: {            
        nome: {
          validators: {
            notEmpty: {
              message: 'Nome é obrigatório. Por favor, insira o nome.'
            }
          }
        },
        telefone: {
          validators: {
            notEmpty: {
              message: 'Telefone é obrigatório. Por favor insira o número de telefone.'
            }
          }
        },
        email: {
          validators: {
            notEmpty: {
              message: 'E-mail é obrigatório. Por favor insira o email.'
            },
            email: {
              message: 'Por favor insira um endereço de email correto.'
            }
          }
        },
        cpf: {
          validators: {
            notEmpty: {
              message: 'CPF obrigatório Por favor, insira o CPF válido.'
            }                    
          }
        }
      }
    })
    .on('success.form.bv', function(e) {
            
      var data = $('#contactform').serialize();
      

       
      return false;
    });
    function resetForm($form) {
      $form.find('input:text, input:password, input, input:file, select, textarea').val('');
      $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
      $form.find('.form-control-feedback').css('display', 'none');
    }



    // Smooth scroll to anchor links
    $(function() {
      $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
          }
        }
      });
    });


})(jQuery);